/* *****************************************************************************
 *  Name: Selva
 *  Date: 09/05/2019
 *  Description: programming assignment 1: PercolationStats.java
 **************************************************************************** */

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;


public class PercolationStats {

    private static final double CONFIDENCE_95 = 1.96;
    private final double mean;
    private final double stddev;
    private final int nbExperiments;

    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0)
            throw new IllegalArgumentException("n is out of bounds");
        if (trials <= 0)
            throw new IllegalArgumentException("trials is out of bounds");

        nbExperiments = trials;
        double[] thresholds = new double[trials];

        int[] sites = new int[n * n];
        for (int i = 0; i < n * n; i++) {
            sites[i] = i;
        }

        for (int i = 0; i < trials; i++) {
            StdRandom.shuffle(sites);
            Percolation percolation = new Percolation(n);
            int j = 0;
            while (j < n * n && !percolation.percolates()) {
                int row = sites[j] / n + 1;
                int col = sites[j] % n + 1;
                percolation.open(row, col);
                j++;
            }
            if (j != n * n)
                thresholds[i] = percolation.numberOfOpenSites() * 1.0 / (n * n);
        }

        mean = StdStats.mean(thresholds);
        stddev = StdStats.stddev(thresholds);
    }

    // sample mean of percolation threshold
    public double mean() {
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return stddev;
    }

    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean - CONFIDENCE_95 * stddev / java.lang.Math.sqrt(nbExperiments);

    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean + CONFIDENCE_95 * stddev / java.lang.Math.sqrt(nbExperiments);
    }

    // test client (described below)
    public static void main(String[] args) {
        PercolationStats ps = new PercolationStats(Integer.parseInt(args[0]),
                                                   Integer.parseInt(args[1]));
        System.out.println("mean                    = " + ps.mean());
        System.out.println("stddev                  = " + ps.stddev());
        System.out.println("95% confidence interval = [" + ps.confidenceLo()
                                   + ", "
                                   + ps.confidenceHi()
                                   + "]");

    }
}
