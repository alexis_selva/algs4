/* *****************************************************************************
 *  Name: Selva
 *  Date: 09/05/2019
 *  Description: programming assignment 1: Percolation.java
 **************************************************************************** */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final int size;
    private int nbOpenSites;
    private boolean[] openSites;
    private final WeightedQuickUnionUF grid;

    // create n-by-n grid, with all sites blocked
    public Percolation(int n) {
        if (n <= 0)
            throw new IllegalArgumentException("n is out of bounds");

        size = n;
        openSites = new boolean[n * n];
        grid = new WeightedQuickUnionUF(n * n + 2);
        for (int i = 1; i <= size; i++) {
            grid.union(size * size, xyTo1D(1, i));
            grid.union(size * size + 1, xyTo1D(size, i));
        }
    }

    //  map 2D coordinates to 1D coordinates
    private int xyTo1D(int row, int col) {
        if (row <= 0 || row > size) return -1;
        if (col <= 0 || col > size) return -1;

        return (row - 1) * size + (col - 1);
    }

    // open site (row, col) if it is not open already
    public void open(int row, int col) {
        int neigh;
        int index = xyTo1D(row, col);
        if (index == -1)
            throw new IllegalArgumentException("index is out of bounds");

        if (openSites[index]) return;
        openSites[index] = true;
        nbOpenSites++;

        neigh = xyTo1D(row - 1, col);
        if (neigh >= 0 && openSites[neigh])
            grid.union(index, neigh);

        neigh = xyTo1D(row + 1, col);
        if (neigh >= 0 && openSites[neigh])
            grid.union(index, neigh);

        neigh = xyTo1D(row, col - 1);
        if (neigh >= 0 && openSites[neigh])
            grid.union(index, neigh);

        neigh = xyTo1D(row, col + 1);
        if (neigh >= 0 && openSites[neigh])
            grid.union(index, neigh);
    }

    // is site (row, col) open?
    public boolean isOpen(int row, int col) {
        int index = xyTo1D(row, col);
        if (index == -1)
            throw new IllegalArgumentException("index is out of bounds");

        return openSites[index];
    }

    // is site (row, col) full?
    public boolean isFull(int row, int col) {
        int index = xyTo1D(row, col);
        if (index == -1)
            throw new IllegalArgumentException("index is out of bounds");

        return openSites[index] && grid.connected(size * size, index);
    }

    // number of open sites
    public int numberOfOpenSites() {
        return nbOpenSites;
    }

    // does the system percolate?
    public boolean percolates() {
        return grid.connected(size * size, size * size + 1);
    }

    // test client (optional)
    public static void main(String[] args) {
        Percolation p = new Percolation(2);
        System.out.println(p.numberOfOpenSites());
        System.out.println(p.percolates());
        p.open(1, 1);
        System.out.println(p.numberOfOpenSites());
        System.out.println(p.percolates());
        p.open(1, 2);
        System.out.println(p.numberOfOpenSites());
        System.out.println(p.percolates());
        p.open(2, 2);
        System.out.println(p.numberOfOpenSites());
        System.out.println(p.percolates());
    }
}
