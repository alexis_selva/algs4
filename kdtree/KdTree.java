/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 20/08/2019
 *  Description: PointSET is a generalization of a BST to two-dimensional keys
 **************************************************************************** */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {
    private static final RectHV TILE = new RectHV(0, 0, 1, 1);
    private Node root;
    private int rootSize;

    private enum Direction {
        VERTICAL,
        HORIZONTAL,
    }

    // construct an empty set of points
    public KdTree() {
    }

    private Direction opposite(Direction d) {
        if (d == Direction.VERTICAL) {
            return Direction.HORIZONTAL;
        }
        else {
            return Direction.VERTICAL;
        }
    }

    // is the set empty?
    public boolean isEmpty() {
        return rootSize == 0;

    }

    // number of points in the set
    public int size() {
        return rootSize;

    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null)
            throw new NullPointerException();
        root = insert(root, p, Direction.VERTICAL);
    }

    private Node insert(Node x, Point2D p, Direction d) {

        if (x == null) {
            rootSize = rootSize + 1;
            return new Node(p, d);
        }

        Point2D nodePoint = x.getPoint();
        if (nodePoint.equals(p)) {
            return x;
        }

        int cmp = x.compareTo(p);
        if (cmp < 0) {
            x.setLeft(insert(x.getLeft(), p, opposite(d)));
        }
        else {
            x.setRight(insert(x.getRight(), p, opposite(d)));
        }
        return x;
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        return getDirection(root, p) != null;
    }

    private Direction getDirection(Node x, Point2D p) {
        if (x == null)
            return null;
        int cmp = x.compareTo(p);
        if (cmp < 0)
            return getDirection(x.getLeft(), p);
        else if (cmp > 0)
            return getDirection(x.getRight(), p);
        else
            return x.getDirection();
    }

    // draw all points to standard draw
    public void draw() {
        draw(root, TILE);
    }

    private void draw(Node x, RectHV rect) {
        if (x != null) {
            if (x.isVertical()) {
                StdDraw.setPenColor(StdDraw.BLACK);
                StdDraw.setPenRadius(0.01);
                x.getPoint().draw();
                StdDraw.setPenColor(StdDraw.RED);
                StdDraw.setPenRadius(0.001);
                StdDraw.line(x.getX(), rect.ymax(), x.getX(), rect.ymin());
            }
            if (!x.isVertical()) {
                StdDraw.setPenColor(StdDraw.BLACK);
                StdDraw.setPenRadius(0.01);
                x.getPoint().draw();
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.setPenRadius(0.001);
                StdDraw.line(rect.xmax(), x.getY(), rect.xmin(), x.getY());
            }
            draw(x.getLeft(), getChildRect(x, rect, true));
            draw(x.getRight(), getChildRect(x, rect, false));
        }
    }

    private static class Node {
        private Point2D p;
        private Direction d;
        private Node left, right;

        public Node(Point2D p, Direction d) {
            this.p = p;
            this.d = d;
        }

        public Point2D getPoint() {
            return this.p;
        }

        public Direction getDirection() {
            return this.d;
        }

        /** Returns the vertical flag. */
        public boolean isVertical() {
            return d == Direction.VERTICAL;
        }

        public Node getLeft() {
            return this.left;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public Node getRight() {
            return this.right;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public int compareTo(Point2D that) {
            if (this.p.y() == that.y() && this.p.x() == that.x())
                return 0;
            if (isVertical() && that.x() < this.p.x())
                return -1;
            if (!isVertical() && that.y() < this.p.y())
                return -1;
            return +1; // right
        }

        public double getX() {
            return this.p.x();
        }

        public double getY() {
            return this.p.y();
        }

    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        Queue<Point2D> jQueue = new Queue<>();
        range(root, TILE, rect, jQueue);
        return jQueue;
    }

    private void range(Node node, RectHV nodeRect, RectHV rect,
                       Queue<Point2D> jQueue) {
        if (node != null && rect.intersects(nodeRect)) {
            if (rect.contains(node.p)) {
                jQueue.enqueue(node.p);
            }
            range(node.getLeft(), getChildRect(node, nodeRect, true), rect, jQueue);
            range(node.getRight(), getChildRect(node, nodeRect, false), rect, jQueue);
        }
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null)
            throw new java.lang.NullPointerException();
        return nearest(root, TILE, p, null, Double.POSITIVE_INFINITY);
    }

    private Point2D nearest(Node node, RectHV rect, Point2D p, Point2D champion,
                            double minDistance) {

        // distance to childRect less than distance to current champion
        if (node != null && rect.distanceSquaredTo(p) < minDistance) {
            // distance from curent node to current point
            double tmpDist = node.getPoint().distanceSquaredTo(p);
            if (tmpDist < minDistance) {
                champion = node.getPoint();
                minDistance = tmpDist;
            }
            int cmp = node.compareTo(p);
            // always check the side where the point's in first
            if (cmp < 0) {
                champion = nearest(node.getLeft(), getChildRect(node, rect, true), p, champion,
                                   minDistance);
                champion = nearest(node.getRight(), getChildRect(node, rect, false), p, champion,
                                   champion.distanceSquaredTo(p));
            }
            else if (cmp > 0) {
                champion = nearest(node.getRight(), getChildRect(node, rect, false), p, champion,
                                   minDistance);
                champion = nearest(node.getLeft(), getChildRect(node, rect, true), p, champion,
                                   champion.distanceSquaredTo(p));
            }
        }
        return champion;
    }

    private RectHV getChildRect(Node x, RectHV rect, boolean left) {
        if (x.isVertical()) {
            if (left) {
                return new RectHV(rect.xmin(), rect.ymin(), x.getX(), rect.ymax());
            }
            else {
                return new RectHV(x.getX(), rect.ymin(), rect.xmax(), rect.ymax());
            }
        }
        else {
            if (left) {
                return new RectHV(rect.xmin(), rect.ymin(), rect.xmax(), x.getY());
            }
            else {
                return new RectHV(rect.xmin(), x.getY(), rect.xmax(), rect.ymax());
            }
        }
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {

    }
}

