/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 20/08/2019
 *  Description: PointSET represents a set of points using red-black BST
 **************************************************************************** */

import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Stack;

public class PointSET {

    private SET<Point2D> set;

    // construct an empty set of points
    public PointSET() {
        set = new SET<Point2D>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return set.isEmpty();
    }

    // number of points in the set
    public int size() {
        return set.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new java.lang.IllegalArgumentException();
        }

        if (!contains(p)) {
            set.add(p);
        }
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new java.lang.IllegalArgumentException();
        }

        return set.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        for (Point2D p : set) {
            p.draw();
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new java.lang.IllegalArgumentException();
        }

        Stack<Point2D> stack = new Stack<Point2D>();
        for (Point2D p : set) {
            // should i check on the rect border? if so - done =)
            if (p.x() >= rect.xmin() && p.x() <= rect.xmax()
                    && p.y() >= rect.ymin() && p.y() <= rect.ymax()) {
                stack.push(p);
            }
        }
        return stack;
    }

    private class Node implements Comparable<Node> {
        private double distance;
        private Point2D point;

        public Node(double d, Point2D p) {
            this.distance = d;
            this.point = p;
        }

        public Point2D getPoint() {
            return point;
        }

        @Override
        public int compareTo(Node that) {
            return Double.compare(this.distance, that.distance);
        }
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new java.lang.IllegalArgumentException();
        }
        if (isEmpty()) {
            return null;
        }

        MinPQ<Node> queue = new MinPQ<Node>();
        for (Point2D that : set) {
            if (that.equals(p)) {
                continue;
            }
            queue.insert(new Node(p.distanceTo(that), that));
        }
        return queue.delMin().getPoint();
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {

    }
}

