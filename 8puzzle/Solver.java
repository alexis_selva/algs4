/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {

    private Board twin;
    private Node current;

    private class Node implements Comparable<Node> {
        private Node previous;
        private Board board;
        private int moves;

        public Node(Board board, int moves, Node previous) {
            this.board = board;
            this.moves = moves;
            this.previous = previous;
        }

        // add moves to simple manhattan
        public int heuristic() {
            return board.manhattan() + moves;
        }

        @Override
        public int compareTo(Node that) {
            return this.heuristic() - that.heuristic();
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        twin = initial.twin();
        MinPQ<Node> priorityQueue = new MinPQ<Node>();

        // Insert the initial search node
        current = new Node(initial, 0, null);
        priorityQueue.insert(current);

        // Dequeue the node with the lowest priority
        current = priorityQueue.delMin();

        // Repeat til the dequeued node is the goal one
        while (!current.board.isGoal()) {
            for (Board n : current.board.neighbors()) {
                if (current.previous == null || !n.equals(current.previous.board)) {
                    Node neighborNode = new Node(n, current.moves + 1, current);
                    priorityQueue.insert(neighborNode);
                }
            }
            if (priorityQueue.isEmpty()) {
                current.moves = -1;
                break;
            }
            current = priorityQueue.delMin();
        }
    }

    // is the initial board solvable?
    public boolean isSolvable() {
        Node firstNode = current;
        StdOut.println(firstNode.board);

        while (firstNode.previous != null) {
            firstNode = firstNode.previous;
        }

        if (firstNode.board.equals(twin)) {
            return false;
        }
        return true;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return current.moves;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        Stack<Board> solution = new Stack<Board>();
        Node firstNode = current;
        solution.push(firstNode.board);
        while (firstNode.previous != null) {
            firstNode = firstNode.previous;
            solution.push(firstNode.board);
        }
        return solution;
    }

    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
