/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;

public class Board {

    private char[] board;
    private int dim;

    // construct a board from an n-by-n array of blocks
    public Board(int[][] blocks) {
        int pos = 0;
        dim = blocks.length;
        board = new char[dim * dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                board[pos++] = (char) blocks[i][j];
            }
        }
    }

    // construct a board from an n-by-n array of blocks
    private Board(Board other) {
        dim = other.dim;
        board = other.board.clone();
    }

    // board dimension n
    public int dimension() {
        return dim;
    }

    // number of blocks out of place
    public int hamming() {
        int num = 0;
        for (int i = 0; i < dim * dim - 1; i++) {
            if (board[i] != i + 1) {
                num++;
            }
        }
        return num;
    }

    private int abs(int a) {
        if (a < 0) {
            return -a;
        }
        return a;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int num = 0;
        for (int i = 0; i < dim * dim; i++) {
            if (board[i] != 0 && board[i] != i + 1) {
                num += (abs((board[i] - (i + 1)) / dim)) + (abs((board[i] - (i + 1)) % dim));
            }
        }
        return num;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    // a board that is obtained by exchanging any pair of blocks
    public Board twin() {
        Board newBoard = new Board(this);
        char tmp;
        int index = 0;
        while (index < dim * dim && board[index] != 0) {
            index++;
        }

        tmp = newBoard.board[index];
        newBoard.board[index] = newBoard.board[index + 1];
        newBoard.board[index + 1] = tmp;
        return newBoard;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;

        Board that = (Board) y;
        if (dim != that.dim) return false;
        for (int i = 0; i < dim * dim; i++) {
            if (board[i] != that.board[i]) {
                return false;
            }
        }

        return true;
    }


    // all neighboring boards
    public Iterable<Board> neighbors() {
        Queue<Board> neighbors = new Queue<Board>();

        int index = 0;
        while (index < dim * dim && board[index] != 0) {
            index++;
        }

        // Move down
        if (index + dim < dim * dim) {
            Board newBoard = new Board(this);
            newBoard.board[index] = newBoard.board[index + dim];
            newBoard.board[index + dim] = 0;
            neighbors.enqueue(newBoard);
        }

        // Move up
        if (index - dim >= 0) {
            Board newBoard = new Board(this);
            newBoard.board[index] = newBoard.board[index - dim];
            newBoard.board[index - dim] = 0;
            neighbors.enqueue(newBoard);
        }

        // Move right
        if ((index + 1 < dim * dim) && (index % dim < (index + 1) % dim)) {
            Board newBoard = new Board(this);
            newBoard.board[index] = newBoard.board[index + 1];
            newBoard.board[index + 1] = 0;
            neighbors.enqueue(newBoard);
        }

        // Move left
        if ((index - 1 >= 0) && ((index - 1) % dim < index % dim)) {
            Board newBoard = new Board(this);
            newBoard.board[index] = newBoard.board[index - 1];
            newBoard.board[index - 1] = 0;
            neighbors.enqueue(newBoard);
        }

        return neighbors;
    }

    // string representation of this board (in the output format specified below)
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(dim + "\n");
        for (int i = 0; i < dim * dim; i++) {
            s.append(String.format("%2d ", (int) board[i]));
            if (i % dim == dim - 1) {
                s.append("\n");
            }
        }
        return s.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();

        Board initial = new Board(blocks);
        System.out.println(initial);

        System.out.println("dimension = " + initial.dimension());
        System.out.println("hamming   = " + initial.hamming());
        System.out.println("manhattan = " + initial.manhattan());

        // Board twin = initial.twin();
        // System.out.println(twin);
        // System.out.println(initial);

        // System.out.println(initial.equals(twin));

        for (Board neigh : initial.neighbors()) {
            System.out.println(neigh);
        }
    }
}
