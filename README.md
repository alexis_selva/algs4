These are the programming assignments relative to Algorithms (Part 1):

- Assignment 1: Percolation
- Assignment 2: Deques and Randomized Queues
- Assignment 3: Collinear Points
- Assignment 4: 8 Puzzle
- Assignment 5: Kd-Trees

For more information, I invite you to have a look at https://www.coursera.org/learn/algorithms-part1
