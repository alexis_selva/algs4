/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 25/05/2019
 *  Description: Permutation
 **************************************************************************** */

import edu.princeton.cs.algs4.StdIn;

public class Permutation {
    public static void main(String[] args) {
        int nb = Integer.parseInt(args[0]);
        RandomizedQueue<String> rqueue = new RandomizedQueue<String>();
        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            rqueue.enqueue(item);
        }
        for (int i = 0; i < nb; i++) {
            String item = rqueue.dequeue();
            System.out.println(item);
        }
    }
}
