/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 25/05/2019
 *  Description: Randomized queue
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] queue;
    private int size;

    // construct an empty randomized queue
    public RandomizedQueue() {
        queue = (Item[]) new Object[2];
        size = 0;
    }

    // resize the underlying array holding the elements
    private void resize(int capacity) {
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            temp[i] = queue[i];
        }
        queue = temp;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return size;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null)
            throw new java.lang.IllegalArgumentException();

        // double size of array if necessary
        if (size == queue.length)
            resize(2 * queue.length);
        
        queue[size++] = item;
    }

    // remove and return a random item
    public Item dequeue() {
        if (size == 0)
            throw new java.util.NoSuchElementException();

        int index = StdRandom.uniform(size);
        Item item = queue[index];
        queue[index] = queue[--size];

        // shrink size of array if necessary
        if (size > 0 && size == queue.length / 4)
            resize(queue.length / 2);

        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (size == 0)
            throw new java.util.NoSuchElementException();

        int index = StdRandom.uniform(size);
        return queue[index];
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {

        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public Item next() {
            if (current == size)
                throw new java.util.NoSuchElementException();

            Item item = queue[current];
            current++;
            return item;
        }

        @Override
        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }
    }

    // unit testing (optional)
    public static void main(String[] args) {
        RandomizedQueue<Integer> rqueue = new RandomizedQueue<Integer>();
        rqueue.enqueue(1);
        rqueue.enqueue(2);
        rqueue.enqueue(3);
        for (int e : rqueue) {
            StdOut.println(e);
        }
        int val = rqueue.dequeue();
        System.out.println("dequeue = " + val);
        for (int e : rqueue) {
            StdOut.println(e);
        }
        rqueue.enqueue(4);
        rqueue.enqueue(5);
        for (int e : rqueue) {
            StdOut.println(e);
        }
        val = rqueue.dequeue();
        System.out.println("dequeue = " + val);
        for (int e : rqueue) {
            StdOut.println(e);
        }
    }
}

