/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 24/05/2019
 *  Description: Dequeue
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }

    private Node first;
    private Node last;
    private int size;

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return first == null;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null)
            throw new java.lang.IllegalArgumentException();

        Node oldfirst = first;
        first = new Node();
        first.item = item;
        first.next = oldfirst;
        if (oldfirst != null)
            oldfirst.prev = first;
        else
            last = first;
        size++;
    }

    // add the item to the end
    public void addLast(Item item) {
        if (item == null)
            throw new java.lang.IllegalArgumentException();

        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.prev = oldlast;
        if (oldlast != null)
            oldlast.next = last;
        else
            first = last;
        size++;
    }

    // TODO remove and return the item from the front
    public Item removeFirst() {
        if (first == null)
            throw new java.util.NoSuchElementException();

        Item item = first.item;
        first = first.next;
        if (first != null)
            first.prev = null;
        else
            last = null;
        size--;
        return item;
    }

    // TODO remove and return the item from the end
    public Item removeLast() {
        if (last == null)
            throw new java.util.NoSuchElementException();

        Item item = last.item;
        last = last.prev;
        if (last != null)
            last.next = null;
        else
            first = null;
        size--;
        return item;
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {

        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (current == null)
                throw new java.util.NoSuchElementException();

            Item item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }
    }

    // unit testing (optional)
    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<Integer>();
        deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);
        for (int e : deque) {
            StdOut.println(e);
        }
        int val = deque.removeLast();
        System.out.println("removeLast = " + val);
        for (int e : deque) {
            StdOut.println(e);
        }
        deque.addLast(4);
        deque.addLast(5);
        for (int e : deque) {
            StdOut.println(e);
        }
        val = deque.removeFirst();
        System.out.println("removeFirst = " + val);
        for (int e : deque) {
            StdOut.println(e);
        }
    }
}


