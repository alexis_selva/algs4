/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 28/05/2019
 *  Description: brute force method to determine colliner points
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

public class BruteCollinearPoints {

    private ArrayList<LineSegment> lines = new ArrayList<LineSegment>();

    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points) {
        if (points == null)
            throw new java.lang.IllegalArgumentException();
        if (points.length == 0)
            throw new java.lang.IllegalArgumentException();
        if (hasNullPoint(points)) {
            throw new IllegalArgumentException();
        }

        Point[] copy = points.clone();

        Arrays.sort(copy);
        if (hasRepeatedPoint(copy)) {
            throw new IllegalArgumentException();
        }

        for (int a = 0; a < copy.length - 3; a++) {
            for (int b = a + 1; b < copy.length - 2; b++) {
                for (int c = b + 1; c < copy.length - 1; c++) {
                    double slope = copy[a].slopeTo(copy[b]);
                    if (slope == copy[a].slopeTo(copy[c])) {
                        for (int d = c + 1; d < copy.length; d++) {
                            if (slope == copy[c].slopeTo(copy[d])) {
                                lines.add(new LineSegment(copy[a], copy[d]));
                            }
                        }
                    }
                }
            }
        }
    }

    // has a null point
    private boolean hasNullPoint(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) {
                return true;
            }
        }
        return false;
    }

    // has a repeated point
    private boolean hasRepeatedPoint(Point[] points) {
        for (int i = 0; i < points.length - 1; i++) {
            if (points[i].compareTo(points[i + 1]) == 0) {
                return true;
            }
        }
        return false;
    }

    // the number of line segments
    public int numberOfSegments() {
        return lines.size();
    }

    // the line segments
    public LineSegment[] segments() {
        return lines.toArray(new LineSegment[lines.size()]);
    }

    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
