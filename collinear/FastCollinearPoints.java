/* *****************************************************************************
 *  Name: Alexis Selva
 *  Date: 09/06/2019
 *  Description: fast method to determine colliner points
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {

    private ArrayList<LineSegment> lines = new ArrayList<LineSegment>();

    // finds all line segments containing 4 or more points
    public FastCollinearPoints(Point[] points) {
        if (points == null)
            throw new java.lang.IllegalArgumentException();
        if (points.length == 0)
            throw new java.lang.IllegalArgumentException();
        if (hasNullPoint(points)) {
            throw new IllegalArgumentException();
        }

        Point[] copy = points.clone();
        
        Arrays.sort(copy);
        if (hasRepeatedPoint(copy)) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < copy.length - 3; i++) {

            Arrays.sort(copy, copy[i].slopeOrder());

            int first = i + 1, last = i + 2;
            while (last < copy.length) {

                // find last collinear to p point
                while (last < copy.length
                        && Double.compare(copy[i].slopeTo(copy[first]),
                                          copy[i].slopeTo(copy[last])) == 0) {
                    last++;
                }

                // if found at least 3 elements, make segment if it's unique
                if (last - first >= 3 && copy[i].compareTo(copy[first]) < 0) {
                    Point[] line = new Point[last - first + 1];
                    for (int j = 0; j < last - first; j++) {
                        line[j] = copy[first + j];
                    }
                    line[last - first] = copy[i];
                    Arrays.sort(line);
                    lines.add(new LineSegment(line[0], line[last - first]));
                }

                // Try to find next
                first = last;
                last++;
            }
        }
    }

    // has a null point
    private boolean hasNullPoint(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) {
                return true;
            }
        }
        return false;
    }

    // has a repeated point
    private boolean hasRepeatedPoint(Point[] points) {
        for (int i = 0; i < points.length - 1; i++) {
            if (points[i].compareTo(points[i + 1]) == 0) {
                return true;
            }
        }
        return false;
    }

    // the number of line segments
    public int numberOfSegments() {
        return lines.size();
    }

    // the line segments
    public LineSegment[] segments() {
        return lines.toArray(new LineSegment[lines.size()]);
    }

    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
